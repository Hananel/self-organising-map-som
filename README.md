# README #

A self-organizing map (SOM) or self-organising feature map (SOFM) is a type of artificial neural network (ANN) that is trained using unsupervised learning to produce a low-dimensional (typically two-dimensional), discretized representation of the input space of the training samples, called a map. Self-organizing maps are different from other artificial neural networks as they apply competitive learning as opposed to error-correction learning (such as backpropagation with gradient descent), and in the sense that they use a neighborhood function to preserve the topological properties of the input space.
[Wiki](https://en.wikipedia.org/wiki/Self-organizing_map)

I search the net and found this great blog [Dynamic Notions](http://dynamicnotions.blogspot.co.il/2008/11/c-self-organising-map-som.html) that have a short nice C# code for Kohonen Network.

The only problem was, the code is not utilize the multithreaded modernize CPU's. So I parallelize the code.