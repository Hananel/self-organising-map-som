// <summary>
// http://dynamicnotions.blogspot.co.il/2008/11/c-self-organising-map-som.html
// </summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

//using Configuration_File;

public class Map
{
	
	private Neuron[] outputs;
	// Collection of weights.
	private int iteration;
	// Current iteration.
	private int length;
	private double nf;
	// Side length of output grid.
	private int dimensions;
	// Number of input dimensions.
	private int samples = 0;
	private int classes = 1;
	private int Number_Total_Of_Neurons = 0;
	private int[] class_start = new int[]{ 0 };
	// counter
	private List<UInt64> time_labbls = new List<UInt64>();
	private List<byte[]> patterns = new List<byte[]>();
	private Dictionary<string, string> dictionary;
	private Random rnd = new Random();
	readonly object _object = new object();
	
	// default values
	private string counber_log = @"counter.csv", labels_log = @"labels.csv";
	private string[] burst_log = new string[]{ @"Burst_Log.csv" };
	private int input_size = 500;
	private int electrode = 64;
	private int Init_Sample_To_Learn = 1000, Max_Sample_Capacity = 2000, Sample_To_Learn = 100;
	private int Max_Iteration;
	private int CPUs = 4;
	private int neurons = 64;
	private int neighbors_distance = 4;
	private bool Learn_In_Random_Order = false;
	private bool Normal_Euclidean_Distances = false;
	private double[] Global_maxError = new double[]{ 0.0001 };
	// maximum error to reach in training
	
	static void Main(string[] args)
	{
		var som = new Map(args);
		//Console.ReadLine();
	}
	
	public int Load_Configuration_File(string fileName)
	{
		dictionary = new Dictionary<string, string>();
		foreach (string line in File.ReadAllLines(fileName)) {
			if ((!string.IsNullOrEmpty(line)) &&
			    (!line.StartsWith(";")) &&
			    (!line.StartsWith("#")) &&
			    (!line.StartsWith("'")) &&
			    (line.Contains("="))) {
				int index = line.IndexOf('=');
				string key = line.Substring(0, index).Trim();
				string value = line.Substring(index + 1).Trim();
				index = value.IndexOf("//");
				if (index >= 0)
					value = value.Substring(0, index);
				index = value.IndexOf(";");
				if (index >= 0)
					value = value.Substring(0, index);
				
				if ((value.StartsWith("/") && value.EndsWith("/")) ||
				    (value.StartsWith("\"") && value.EndsWith("\"")) ||
				    (value.StartsWith("'") && value.EndsWith("'"))) {
					value = value.Substring(1, value.Length - 2);
				}
				dictionary.Add(key, value);
			}
		}
		
		//---------------------------------------------------------------
		
		string temp;
		bool failer = false;
		
		dictionary.TryGetValue("neurons", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - neurons");
			failer = true;
		} else
			this.neurons = Convert.ToInt32(temp);
		//this.neurons = Convert.ToUInt32(temp);
		
		dictionary.TryGetValue("input_size", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - input_size");
			failer = true;
		} else
			this.input_size = Convert.ToInt32(temp);
		
		dictionary.TryGetValue("electrode", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - electrode");
			failer = true;
		} else
			this.electrode = Convert.ToInt32(temp);
		
		dictionary.TryGetValue("CPUs", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - CPUs");
			failer = true;
		} else
			this.CPUs = Convert.ToInt32(temp);
		
		dictionary.TryGetValue("maxError", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - maxError");
			failer = true;
		} else {
			string[] parts = temp.Split(',');
			this.Global_maxError = new double[parts.Length];
			for (int i = 0; i < parts.Length; i++)
				this.Global_maxError[i] = Convert.ToDouble(parts[i]);
		}
		
		dictionary.TryGetValue("Init_Sample_To_Learn", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - Max_Sample_To_Learn");
			failer = true;
		} else
			this.Init_Sample_To_Learn = Convert.ToInt32(temp);
		
		dictionary.TryGetValue("Max_Sample_Capacity", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - Max_Sample_Capacity");
			failer = true;
		} else
			this.Max_Sample_Capacity = Convert.ToInt32(temp);
		
		dictionary.TryGetValue("Sample_To_Learn", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - Sample_To_Learn");
			failer = true;
		} else
			this.Sample_To_Learn = Convert.ToInt32(temp);
		
		dictionary.TryGetValue("burst_log", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - burst_log");
			failer = true;
		} else {
			string[] parts = temp.Split(',');
			this.burst_log = new string[parts.Length];
			for (int i = 0; i < parts.Length; i++)
				this.burst_log[i] = @parts[i];
			classes = parts.Length;
		}
		
		dictionary.TryGetValue("counber_log", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - counber_log");
			failer = true;
		} else
			this.counber_log = @temp;
		
		dictionary.TryGetValue("labels_log", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - labels_log");
			failer = true;
		} else
			this.labels_log = @temp;
		
		dictionary.TryGetValue("Learn_In_Random_Order", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - Learn_In_Random_Order");
			failer = true;
		} else
			this.Learn_In_Random_Order = Convert.ToBoolean(temp);
		
		dictionary.TryGetValue("Normal_Euclidean_Distances", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - Normal_Euclidean_Distances");
			failer = true;
		} else
			this.Normal_Euclidean_Distances = Convert.ToBoolean(temp);
		
		
		dictionary.TryGetValue("Neighbors_Distance", out temp);
		if (string.IsNullOrEmpty(temp)) {
			Console.WriteLine("Didnt found - Neighbors_Distance");
			failer = true;
		} else
			this.neighbors_distance = Convert.ToInt32(temp);
		
		
		//---------------------------------------
		if (failer) {
			Console.WriteLine("Problem with configuration");
			return -1;
		} else {
			Console.WriteLine("Configuration file loaded");
			return 0;
		}
	}
	
	public Map(string[] args)
	{
		bool conf = true;
		if (args.Length > 0) {
			int r = Load_Configuration_File(args[0]);
			if (r < 0)
				conf = false;
		} else
			Console.WriteLine("Configuration file NOT LOADED!");
		
		if (conf) {
			
			Initialise();
			
			string counber_log_org = counber_log.ToString();
			
			for (int t = 0; t < this.Global_maxError.Length; t++) {
				iteration = 0;
				counber_log = counber_log_org + "_of_global_map_" + this.Global_maxError[t].ToString();
				StreamReader reader = File.OpenText(this.burst_log[0]);
				var counter_writer = new StreamWriter(this.counber_log + ".csv");
				var labels_writer = new StreamWriter(this.labels_log + ".csv");
				bool eof = LoadData(ref reader, Init_Sample_To_Learn);
				
				Backup_Neurons_Weights();
				
				do {
					Train(this.Global_maxError[t]);
					
					Dump_Counters_and_Labbles(ref counter_writer, ref labels_writer);
					
					if (this.burst_log.Length == 1)
						eof = LoadData(ref reader, Sample_To_Learn);
					
					iteration = Max_Iteration - Sample_To_Learn;
					
					Console.WriteLine(" Load " + Sample_To_Learn.ToString() + " samples, iteration " + iteration.ToString());
					
					//Restore_Neurons_Weights();
					
				} while(eof);
				
				counter_writer.Close();
				labels_writer.Close();
				reader.Close();

				// restore weights of the neurons to the begininng
				Restore_Neurons_Weights();
			}
			
		}
		
	}
	
	private void Initialise()
	{
		this.length = this.neurons;
		this.dimensions = this.input_size * this.electrode;
		
		Max_Iteration = Max_Sample_Capacity == 0 ? Init_Sample_To_Learn : Max_Sample_Capacity;
		patterns.Capacity = Max_Sample_Capacity == 0 ? 1000000 : Max_Sample_Capacity;
		double learning_ratio = Sample_To_Learn / Max_Iteration;
		
		this.nf = (double)Max_Iteration / Math.Log(length);
		this.Number_Total_Of_Neurons = length * length;
		
		//-------------------------------
		
		outputs = new Neuron[Number_Total_Of_Neurons];
		for (int e = 0, i = 0, j = 0;
		     e < Number_Total_Of_Neurons;
		     e++, i = e / length , j = e % length) {
			outputs[e] = new Neuron(i, j, e);
			outputs[e].Weights = new double[dimensions];
			outputs[e].double_Weights = new double[dimensions];
			outputs[e].Weights_backup = new double[dimensions];
			for (int k = 0; k < dimensions; k++) {
				double r = rnd.Next() /(int.MaxValue + 0.0);
				outputs[e].Weights[k] = r;
				outputs[e].double_Weights[k] = r * r;
				outputs[e].Weights_backup[k] = r;
			}
			
			// distance
			outputs[e].distance = new double[length, length];
			for (int x = 0, id =0; x < length; x++) {
				for (int y = 0; y < length; y++) {
					int tX = x - i;
					int tY = y - j;
					double temp = Math.Sqrt((tX * tX) + (tY * tY));
					outputs[e].distance[x, y] = -(temp * temp);
					//Console.WriteLine("id= "+ id.ToString() + " X= " + i.ToString() + " Y= " + j.ToString() + " x= " + x.ToString() + " y= "+ y.ToString()+" dist= "+outputs[e].distance[x, y].ToString());
					id++;
				}
			}
			
		}
		
		//  Neighbors for each neuron
		foreach (Neuron n in outputs) {
			int num = 0;
			for (int x = 0; x < length; x++)
				for (int y = 0; y < length; y++)
					if (n.distance[x, y] >= -neighbors_distance)
						num++;
			n.neighbors = new Neuron[num];
			
			int i = 0, c = 0;
			for (int x = 0; x < length; x++)
				for (int y = 0; y < length; y++) {
				if (n.distance[x, y] >= -neighbors_distance) {
					n.neighbors[i] = outputs[c];
					i++;
				}
				c++;
			}
		}
	}
	
	private bool LoadData(ref StreamReader reader, int lines)
	{
		if (classes == 1) {  // lead by time
			int count = 0;
			while (!reader.EndOfStream) {
				string[] line = reader.ReadLine().Split(',');
				var time_lable = UInt64.Parse(line[0]);
				var inputs = new byte[dimensions];
				int e = -1;
				for (int i = 1; i < line.Length; i++) {
					if (line[i].CompareTo("") == 0)
						continue;
					var p = Int32.Parse(line[i]);
					if (p <= 0)
						e = -p;
					else
						inputs[e * input_size + p] = 1;
				}
				if (Max_Sample_Capacity > 0 && samples == Max_Sample_Capacity) {
					patterns.RemoveAt(0);
					patterns.Add(inputs);
					time_labbls.RemoveAt(0);
					time_labbls.Add(time_lable);
				} else {
					patterns.Add(inputs);
					time_labbls.Add(time_lable);
					samples++;
				}
				count++;
				if (count == lines)
					break;
			}
			
		} else {  // load all classes
			
			class_start = new int[classes];
			int count = 0;
			
			for (int f = 0; f < classes; f++) {
				class_start[f] = count;
				reader = new StreamReader(burst_log[f]);
				while (!reader.EndOfStream) {
					string[] line = reader.ReadLine().Split(',');
					var time_lable = UInt64.Parse(line[0]);
					var inputs = new byte[dimensions];
					int e = -1;
					for (int i = 1; i < line.Length; i++) {
						if (line[i].CompareTo("") == 0)
							continue;
						var p = Int32.Parse(line[i]);
						if (p <= 0)
							e = -p;
						else
							inputs[e * input_size + p] = 1;
					}
					count++;
					patterns.Add(inputs);
					time_labbls.Add(time_lable);
				}
			}
			
		}
		
		return (!reader.EndOfStream);
	}
	
	private void Train(double maxError)
	{
		double currentError = 0;
		
		var options = new ParallelOptions();
        options.MaxDegreeOfParallelism = CPUs;
		
		do {
			Console.WriteLine();
			currentError = 0;
			
			if (Learn_In_Random_Order) {
				var TrainingSet = new List<byte[]>();
				foreach (byte[] pattern in patterns)
					TrainingSet.Add(pattern);

				for (int i = 0; i < patterns.Count; i++) {

					if (i % 50 == 0)
						Console.Write(".");

					int place = rnd.Next(TrainingSet.Count - 1);
					byte[] pattern = TrainingSet[place];
					currentError += TrainPattern(pattern, ref options);
					TrainingSet.RemoveAt(place);
				}
			} else {
				int c = 0;
				foreach (byte[] pattern in patterns) {
					currentError += TrainPattern(pattern, ref options);
					if (c % 50 == 0)
						Console.Write(".");
					c++;
				}
			}
			
			Console.Write("iteration: " + iteration.ToString() + " Error: " + currentError.ToString("0.0000000"));
		} while (currentError > maxError);
	}
	
	private double TrainPattern(byte[] pattern,ref ParallelOptions options)
	{
		Neuron winner = (Normal_Euclidean_Distances)? Winner_Normal_Euclidean(pattern) : Winner_Customize_Euclidean(pattern);
		int winnerX = winner.X, winnerY = winner.Y;
		
		double LearningRate = Math.Exp(-iteration / (Max_Iteration+0.0)) * 0.1;
		double strength = Math.Exp(-iteration / nf) * (double)length;
		strength *= strength;
		
		int e = winner.neighbors.Length;
		var error = new double[e];
		
		Parallel.For(0, e,options, n => {
		             	Neuron N = winner.neighbors[n];
		             	double t = (LearningRate * Math.Exp(N.distance[winnerX, winnerY] / strength));
		             	error[n] = N.UpdateWeights(pattern, t);
		             });
//		for (int n = 0; n < e; n++){
//		             	Neuron N = winner.neighbors[n];
//		             	double LR = (LearningRate * Math.Exp(N.distance[winnerX, winnerY] / strength));
//	//		             	Console.WriteLine("Win: "+winner.ID.ToString() + " Ne:" + N.ID.ToString() +
//	//		             	                  " Learning Rate:" + LearningRate.ToString() + " Strengh " + strength.ToString() +
//	//		             	                  " Distance " + N.distance[winnerX,winnerY].ToString() + " LearningR_Gate "+ LR.ToString());
//		             	error[n] = N.UpdateWeights(pattern, LR );
//		             }
		
		//if (iteration < Max_Iteration)
		iteration++;
		//return Math.Abs(error.AsParallel().Sum() / (double) e);
		
		double r = error[0];
		for (int i = 1; i < e; i++) {
			r+=error[i];
		}
		return Math.Abs(r / (double) e);
	}
	
	private void Dump_Counters_and_Labbles(ref StreamWriter counter_writer, ref StreamWriter labble_writer)
	{
		
		var labbles = new List<UInt64>[length, length];
		for (int x = 0; x < length; x++)
			for (int y = 0; y < length; y++)
				labbles[x, y] = new List<UInt64>();
		
		var global_counter = new int[length, length];
		var class_counters = new List<int[,]>();
		for (int c = 0; c < classes; c++)
			class_counters.Add(new int[length, length]);
		
		for (int c = 0, i = 0; i < patterns.Count; i++) {
			Neuron n = (Normal_Euclidean_Distances)? Winner_Normal_Euclidean(patterns[i]) : Winner_Customize_Euclidean(patterns[i]);
			labbles[n.X, n.Y].Add(time_labbls[i]);
			global_counter[n.X, n.Y]++;
			
			if ((c + 1 < classes) && (class_start[c + 1] < i))
				c++;
			class_counters[c][n.X, n.Y]++;
		}
		
		
		// write the counters
		for (int x = 0; x < length; x++) {
			for (int y = 0; y < length; y++) {
				labble_writer.Write("{0},{1}", x, y);
				for (int i = 0; i < labbles[x, y].Count; i++)
					labble_writer.Write(",{0}", labbles[x, y][i]);
				labble_writer.WriteLine();
			}
		}
//		for (int w = 0; w < dimensions; w++) {
//			for (int n = 0; n < length *length; n++)
//				labble_writer.Write(outputs[n].Weights[w].ToString() + ",");
//			labble_writer.WriteLine();
//		}
		
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < length; j++)
				counter_writer.Write(global_counter[i, j].ToString() + ",");
			counter_writer.WriteLine();
		}
		counter_writer.Flush();
		
		if (classes > 1) {
			for (int c = 0; c < classes; c++) {
				var wr = new StreamWriter(counber_log + "_class_" + c.ToString() + ".csv", true);
				for (int i = 0; i < length; i++) {
					for (int j = 0; j < length; j++)
						wr.Write(class_counters[c][i, j].ToString() + ",");
					wr.WriteLine();
				}
				wr.Flush();
				wr.Close();
			}
		}
		
	}
	
	private Neuron Winner_Customize_Euclidean(byte[] pattern)
	{
		var winner = new Neuron[this.Number_Total_Of_Neurons];
		var min = new double[this.Number_Total_Of_Neurons];

		Parallel.ForEach(outputs,new ParallelOptions { MaxDegreeOfParallelism = CPUs }, n => {
		                 	// find the Euclidean distance
		                 	double[] weights = n.Weights;
		                 	double dd = 0;
		                 	for (int i = 0; i < this.input_size; i++) {
		                 		double t, d = 0;
		                 		for (int w = i * this.electrode, end = w + this.electrode; w < end; w++) {
		                 			if (pattern[w]==0){
		                 				d += n.double_Weights[w];
		                 			}else{
		                 				t = pattern[w] - weights[w];
		                 				d += t * t;
		                 			}
		                 		}
		                 		dd += Math.Sqrt(d);
		                 	}
		                 	//
		                 	min[n.ID] = dd;
		                 	winner[n.ID] = n;
		                 });
		int idx = 0;
		for (int i = 0; i < this.Number_Total_Of_Neurons; i++)
			if (winner[i] != null) {
			idx = i;
			break;
		}

		for (int i = idx + 1; i < this.Number_Total_Of_Neurons; i++)
			if ((winner[i] != null) && (min[i] < min[idx]))
				idx = i;

		return winner[idx];
		
	}
	
	private Neuron Winner_Normal_Euclidean(byte[] pattern)
	{
		var winner = new Neuron[this.Number_Total_Of_Neurons];
		var min = new double[this.Number_Total_Of_Neurons];

		Parallel.ForEach(outputs,new ParallelOptions { MaxDegreeOfParallelism = CPUs }, n => {
		                 	// find the Euclidean distance
		                 	double t, d = 0;
		                 	double[] weights = n.Weights;
		                 	for (int w = 0; w < weights.Length; w++) {
		                 		t = (double)pattern[w] - weights[w];
		                 		d += t * t;
		                 	}
		                 	//
		                 	min[n.ID] = Math.Sqrt(d);
		                 	winner[n.ID] = n;
		                 });
		int idx = 0;
		for (int i = 0; i < this.Number_Total_Of_Neurons; i++)
			if (winner[i] != null) {
			idx = i;
			break;
		}

		for (int i = idx + 1; i < this.Number_Total_Of_Neurons; i++)
			if ((winner[i] != null) && (min[i] < min[idx]))
				idx = i;

		return winner[idx];
	}

	private void Backup_Neurons_Weights(){
		Console.WriteLine("  - Backup Neurons Weights -  ");
		Parallel.ForEach(outputs, n => {
		                 	n.Backup_Weights();
		                 });
	}
	
	private void Restore_Neurons_Weights(){
		Console.WriteLine("  - Restore Neurons Weights -  ");
		Parallel.ForEach(outputs, n => {
		                 	n.Restore_Weights_backup();
		                 });
	}
	
	
}

public class Neuron
{
	public double[] Weights,double_Weights, Weights_backup;
	public double[,] distance;
	public Neuron[] neighbors;
	public int X;
	public int Y;
	public int ID;
	
	public Neuron(int x, int y, int id)
	{
		X = x;
		Y = y;
		ID = id;
	}
	
	public void Restore_Weights_backup()
	{
		for (int k = 0; k < Weights.Length; k++){
			Weights[k] = Weights_backup[k];
			double_Weights[k] = Weights_backup[k] * Weights_backup[k];
		}
	}
	
	public void Backup_Weights()
	{
		for (int k = 0; k < Weights.Length; k++)
			Weights_backup[k] = Weights[k];
	}
	
	
	public double UpdateWeights(byte[] pattern, double LearningRate_Gauss)
	{
		double sum = 0;
		
		for (int i = 0; i < Weights.Length; i++) {
			double delta = LearningRate_Gauss * (pattern[i] - Weights[i]);
			Weights[i] += delta;
			double_Weights[i] = Weights[i] * Weights[i];
			sum += delta;
		}
		
		return sum / Weights.Length;
	}
}